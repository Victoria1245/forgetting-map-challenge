package uk.co.vsf.challenge.forgettingmap;

import java.util.Comparator;
import java.util.Objects;

class ValueAccessedCounterWrapper<T> {
	private long counter;
	private final T value;
	private final long creationTimestamp = System.currentTimeMillis();
	private final long comparatorTieBreaker;

	ValueAccessedCounterWrapper(final T value) {
		this.comparatorTieBreaker = 1L;
		this.value = value;
	}

	public ValueAccessedCounterWrapper(final long comparatorTieBreaker, final T value) {
		this.comparatorTieBreaker = comparatorTieBreaker;
		this.value = value;
	}

	public T getAndIncrement() {
		counter += 1L;
		return this.value;
	}

	public T getNoIncrement() {
		return this.value;
	}

	public long getCounter() {
		return this.counter;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueAccessedCounterWrapper other = (ValueAccessedCounterWrapper) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("counter='%d', timestamp='%d', comparatorTieBreaker='%d', value='%s'", this.counter,
				this.creationTimestamp, this.comparatorTieBreaker, Objects.toString(value));
	}

	public static <T> Comparator<ValueAccessedCounterWrapper<T>> getComparator() {
		return new Comparator<ValueAccessedCounterWrapper<T>>() {
			@Override
			public int compare(ValueAccessedCounterWrapper<T> o1, ValueAccessedCounterWrapper<T> o2) {
				int counterComparison = Long.compare(o1.counter, o2.counter);
				if (counterComparison == 0) {
					int creationTimestampComparison = Long.compare(o1.creationTimestamp, o2.creationTimestamp);
					if (creationTimestampComparison == 0) {
						return Long.compare(o1.comparatorTieBreaker, o2.comparatorTieBreaker);
					}

					return creationTimestampComparison;
				}

				return counterComparison;
			}
		};
	}
}
