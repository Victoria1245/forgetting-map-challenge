package uk.co.vsf.challenge.forgettingmap;

import java.util.Optional;

/**
 * A ForgettingMap is similar to that of a standard Map but will forget the
 * least accessed item within the Map if a new item is added to the Map and it's
 * already full. If there is a tie breaker between two or entries, the item
 * added to the map first will be removed.
 * 
 * @param <K> Key type
 * @param <V> Value type
 */
public interface ForgettingMap<K, V> {

	/**
	 * Find an item in the forgetting map by its key.
	 * 
	 * @param key to find the item by
	 * @return an Optional of type V.
	 */
	Optional<V> find(K key);

	/**
	 * Adds a new item to the ForgettingMap. If the Map is already full, the new
	 * item being added will force the Map to discard the least accessed item
	 * currently in the Map before adding the new item.
	 * 
	 * @param key   of the item to add to the Map
	 * @param value of the item to add to the Map
	 */
	void add(K key, V value);

	/**
	 * Returns the count of items being stored in the ForgettingMap.
	 * 
	 * @return count of items in the Map
	 */
	long count();

}
