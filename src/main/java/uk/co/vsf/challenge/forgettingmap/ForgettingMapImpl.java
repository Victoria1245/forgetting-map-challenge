package uk.co.vsf.challenge.forgettingmap;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class ForgettingMapImpl<K, V> implements ForgettingMap<K, V> {

	private final long maxEntries;
	private final AtomicLong comparatorTieBreaker = new AtomicLong(Long.MIN_VALUE);
	private final Map<K, ValueAccessedCounterWrapper<V>> map = new HashMap<>();

	public ForgettingMapImpl(final long maxEntries) {
		validateMaxEntries(maxEntries);

		this.maxEntries = maxEntries;
	}

	private void validateMaxEntries(final long maxEntries) {
		if (maxEntries < 1) {
			throw new IllegalArgumentException("MaxEntries must be greater than zero");
		}
	}

	@Override
	public void add(final K key, final V value) {
		synchronized (map) {
			if (isMapFull()) {
				if (doesMapAlreadyContainsKey(key)) {
					addToMap(key, value);
				} else {
					removeLeastAccessedKey();
					addToMap(key, value);
				}
			} else {
				addToMap(key, value);
			}
		}
	}

	private boolean doesMapAlreadyContainsKey(final K key) {
		return map.containsKey(key);
	}

	private boolean isMapFull() {
		return map.size() == maxEntries;
	}

	private void removeLeastAccessedKey() {
		final Comparator<ValueAccessedCounterWrapper<V>> comparator = ValueAccessedCounterWrapper.getComparator();

		Optional<Entry<K, ValueAccessedCounterWrapper<V>>> toBeRemoved = this.map.entrySet().stream().sorted((a, b) -> {
			return comparator.compare(a.getValue(), b.getValue());
		}).findFirst();

		this.map.remove(toBeRemoved.orElseThrow().getKey());
	}

	private void addToMap(final K key, final V value) {
		ValueAccessedCounterWrapper<V> valueWrapper = new ValueAccessedCounterWrapper<>(
				comparatorTieBreaker.getAndIncrement(), value);
		this.map.put(key, valueWrapper);
	}

	@Override
	public Optional<V> find(final K key) {
		synchronized (map) {
			ValueAccessedCounterWrapper<V> valueWrapper = map.get(key);
			if (valueWrapper == null) {
				return Optional.empty();
			} else {
				return Optional.of(valueWrapper.getAndIncrement());
			}
		}
	}

	@Override
	public long count() {
		synchronized (map) {
			return this.map.keySet().parallelStream().count();
		}
	}

	public boolean contains(final K key, final V value) {
		synchronized (map) {
			final ValueAccessedCounterWrapper<V> valueWrapper = new ValueAccessedCounterWrapper<>(value);
			return this.map.entrySet().stream()
					.anyMatch(e -> e.getKey().equals(key) && Objects.equals(e.getValue(), valueWrapper));
		}
	}

	@Override
	public String toString() {
		synchronized (map) {
			return this.map.toString();
		}
	}
}
