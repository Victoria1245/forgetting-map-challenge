package uk.co.vsf.challenge.forgettingmap;

public class KeyValuePair<K, V> {
	private final K key;
	private final V value;

	public KeyValuePair(K key, V value) {
		super();
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("KeyValuePair[key='%s', value='%s']", key.toString(), value.toString());
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}
}
