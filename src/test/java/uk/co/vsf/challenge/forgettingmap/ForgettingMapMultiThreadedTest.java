package uk.co.vsf.challenge.forgettingmap;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class ForgettingMapMultiThreadedTest {

	@Test
	public void testAddRandomValues() {
		ForgettingMap<Integer, Integer> forgettingMap = new ForgettingMapImpl<>(10);
		Random r = new Random();

		Stream.generate(() -> new KeyValuePair<Integer, Integer>(r.nextInt(), r.nextInt())).limit(10000).parallel()
				.forEach(kvPair -> forgettingMap.add(kvPair.getKey(), kvPair.getValue()));

		assertEquals(10, forgettingMap.count());
	}

	@Test
	public void addAndFetchRandomValues() throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(10);

		ForgettingMap<Integer, Integer> forgettingMap = new ForgettingMapImpl<>(10);
		Random r = new Random();

		Collection<Callable<Object>> work = new ArrayList<>();
		for (int i = 0; i < 1000000; i++) {
			Integer key = r.nextInt();
			if (key % 3 == 1) {
				work.add(new Callable<Object>() {

					@Override
					public Object call() throws Exception {
						forgettingMap.find(key);
						return null;
					}
				});
			}

			work.add(new Callable<Object>() {

				@Override
				public Object call() throws Exception {
					forgettingMap.add(key, r.nextInt());
					return null;
				}
			});
		}

		Collections.shuffle((List<?>) work);

		executorService.invokeAll(work);
		executorService.shutdown();

		while (!executorService.isShutdown()) {
			System.out.println("Waiting for all work items to complete");
			Thread.sleep(1000);
		}

		assertEquals(10, forgettingMap.count());
	}
}
