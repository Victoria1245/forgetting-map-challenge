package uk.co.vsf.challenge.forgettingmap;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;

public class ForgettingMapSingleThreadedTest {

	private <K, V> void addToMapAndAssertMapCount(ForgettingMap<K, V> map, KeyValuePair<K, V> kvPair,
			int expectedCount) {
		map.add(kvPair.getKey(), kvPair.getValue());
		assertEquals(expectedCount, map.count());
	}

	private <K, V> void assertMapContainsKeyValuePair(ForgettingMap<K, V> map, KeyValuePair<K, V> kvPair) {
		assertTrue(((ForgettingMapImpl<K, V>) map).contains(kvPair.getKey(), kvPair.getValue()));
	}

	private <K, V> void assertFindsEntry(ForgettingMap<K, V> map, KeyValuePair<K, V> kvPair) {
		Optional<V> findResult = map.find(kvPair.getKey());
		assertEquals(kvPair.getValue(), findResult.get());
	}

	private final KeyValuePair<String, String> stringPairOne = new KeyValuePair<>("abc", "def");
	private final KeyValuePair<String, String> stringPairTwo = new KeyValuePair<>("ghi", "jkl");
	private final KeyValuePair<String, String> stringPairThree = new KeyValuePair<>("mno", "pqr");
	private final KeyValuePair<String, String> stringPairFour = new KeyValuePair<>("stu", "vwx");
	private final KeyValuePair<String, String> stringPairFive = new KeyValuePair<>("yz1", "234");
	private final KeyValuePair<Integer, Integer> integerPairOne = new KeyValuePair<>(1, 2);
	private final KeyValuePair<Integer, Integer> integerPairTwo = new KeyValuePair<>(3, 4);
	private final KeyValuePair<Integer, Integer> integerPairThree = new KeyValuePair<>(11, 12);
	private final KeyValuePair<Integer, Integer> integerPairFour = new KeyValuePair<>(444, 555);
	private final KeyValuePair<Integer, Integer> integerPairFive = new KeyValuePair<>(7, -1);
	private final KeyValuePair<Integer, Integer> integerPairSix = new KeyValuePair<>(0, -1);
	private final KeyValuePair<Integer, Integer> integerPairSeven = new KeyValuePair<>(346, -444);
	private final KeyValuePair<Integer, Integer> integerPairEight = new KeyValuePair<>(-4, 490);
	private final KeyValuePair<Integer, Integer> integerPairNine = new KeyValuePair<>(22, 4);
	private final KeyValuePair<Integer, Integer> integerPairTen = new KeyValuePair<>(946, 5315);
	private final KeyValuePair<String, Integer> stringIntegerPairOne = new KeyValuePair<>("a", 2);
	private final KeyValuePair<String, Integer> stringIntegerPairTwo = new KeyValuePair<>("b", 4);
	private final KeyValuePair<String, Integer> stringIntegerPairThree = new KeyValuePair<>("c", 2);
	private final KeyValuePair<String, Integer> stringIntegerPairFour = new KeyValuePair<>("d", 555);
	private final KeyValuePair<String, Integer> stringIntegerPairFive = new KeyValuePair<>("e", -1);
	private final KeyValuePair<String, Integer> stringIntegerPairSix = new KeyValuePair<>("f", -1);
	private final KeyValuePair<String, Integer> stringIntegerPairSeven = new KeyValuePair<>("g", -444);
	private final KeyValuePair<String, Integer> stringIntegerPairEight = new KeyValuePair<>("c", 77);

	@Test
	public void constructWithNegativeNumber_ThrowsException() {
		assertThrows(IllegalArgumentException.class, () -> new ForgettingMapImpl<String, String>(-2),
				"MaxEntries must be greater than zero");
	}

	@Test
	public void constructWithNZero_ThrowsException() {
		assertThrows(IllegalArgumentException.class, () -> new ForgettingMapImpl<String, String>(0),
				"MaxEntries must be greater than zero");
	}

	@Test
	public void basicAddItemToMapSuccessful() {
		ForgettingMap<String, String> map = new ForgettingMapImpl<>(2);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringPairOne, 1);
	}

	@Test
	public void addItemsToMap_Size2_SecondAndThirdItemsRemaining() {

		ForgettingMap<String, String> map = new ForgettingMapImpl<>(2);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringPairOne, 1);
		addToMapAndAssertMapCount(map, stringPairTwo, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairTwo);

		addToMapAndAssertMapCount(map, stringPairThree, 2);

		assertMapContainsKeyValuePair(map, stringPairTwo);
		assertMapContainsKeyValuePair(map, stringPairThree);
	}

	@Test
	public void addItemsToMap_Size2_FirstAndThirdItemsRemaining() {
		ForgettingMap<String, String> map = new ForgettingMapImpl<>(2);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringPairOne, 1);
		addToMapAndAssertMapCount(map, stringPairTwo, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairTwo);

		assertFindsEntry(map, stringPairOne);

		addToMapAndAssertMapCount(map, stringPairThree, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairThree);
	}

	@Test
	public void addItemsToMap_Size2_FirstAndFourthItemsRemaining() {
		ForgettingMap<String, String> map = new ForgettingMapImpl<>(2);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringPairOne, 1);
		addToMapAndAssertMapCount(map, stringPairTwo, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairTwo);

		assertFindsEntry(map, stringPairOne);

		addToMapAndAssertMapCount(map, stringPairThree, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairThree);

		assertFindsEntry(map, stringPairOne);

		addToMapAndAssertMapCount(map, stringPairFour, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairFour);
	}

	@Test
	public void addItemsToMap_Size2_ThirdAndFourthItemsRemaining() {
		ForgettingMap<String, String> map = new ForgettingMapImpl<>(2);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringPairOne, 1);
		addToMapAndAssertMapCount(map, stringPairTwo, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairTwo);

		assertFindsEntry(map, stringPairOne);

		addToMapAndAssertMapCount(map, stringPairThree, 2);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairThree);

		assertFindsEntry(map, stringPairOne);
		assertFindsEntry(map, stringPairThree);
		assertFindsEntry(map, stringPairThree);
		assertFindsEntry(map, stringPairThree);

		addToMapAndAssertMapCount(map, stringPairFour, 2);

		assertMapContainsKeyValuePair(map, stringPairThree);
		assertMapContainsKeyValuePair(map, stringPairFour);
	}

	@Test
	public void addItemsToMap_Size3_FirstThirdAndFourthItemsRemaining() {

		ForgettingMap<String, String> map = new ForgettingMapImpl<>(3);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringPairOne, 1);
		addToMapAndAssertMapCount(map, stringPairTwo, 2);
		addToMapAndAssertMapCount(map, stringPairThree, 3);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairTwo);
		assertMapContainsKeyValuePair(map, stringPairThree);

		assertFindsEntry(map, stringPairOne);
		assertFindsEntry(map, stringPairTwo);
		assertFindsEntry(map, stringPairThree);
		assertFindsEntry(map, stringPairOne);

		addToMapAndAssertMapCount(map, stringPairFour, 3);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairThree);
		assertMapContainsKeyValuePair(map, stringPairFour);
	}

	@Test
	public void addItemsToMap_Size3_FirstThirdAndFifthItemsRemaining() {

		ForgettingMap<String, String> map = new ForgettingMapImpl<>(3);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringPairOne, 1);
		addToMapAndAssertMapCount(map, stringPairTwo, 2);
		addToMapAndAssertMapCount(map, stringPairThree, 3);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairTwo);
		assertMapContainsKeyValuePair(map, stringPairThree);

		assertFindsEntry(map, stringPairOne);
		assertFindsEntry(map, stringPairTwo);
		assertFindsEntry(map, stringPairThree);
		assertFindsEntry(map, stringPairOne);

		addToMapAndAssertMapCount(map, stringPairFour, 3);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairThree);
		assertMapContainsKeyValuePair(map, stringPairFour);

		addToMapAndAssertMapCount(map, stringPairFive, 3);

		assertMapContainsKeyValuePair(map, stringPairOne);
		assertMapContainsKeyValuePair(map, stringPairThree);
		assertMapContainsKeyValuePair(map, stringPairFive);
	}

	@Test
	public void addItemsToMap_Size5_Integers() {
		ForgettingMap<Integer, Integer> map = new ForgettingMapImpl<>(5);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, integerPairOne, 1);
		addToMapAndAssertMapCount(map, integerPairTwo, 2);
		addToMapAndAssertMapCount(map, integerPairThree, 3);
		addToMapAndAssertMapCount(map, integerPairFour, 4);
		addToMapAndAssertMapCount(map, integerPairFive, 5);

		assertMapContainsKeyValuePair(map, integerPairOne);
		assertMapContainsKeyValuePair(map, integerPairTwo);
		assertMapContainsKeyValuePair(map, integerPairThree);
		assertMapContainsKeyValuePair(map, integerPairFour);
		assertMapContainsKeyValuePair(map, integerPairFive);

		addToMapAndAssertMapCount(map, integerPairSix, 5);

		assertMapContainsKeyValuePair(map, integerPairTwo);
		assertMapContainsKeyValuePair(map, integerPairThree);
		assertMapContainsKeyValuePair(map, integerPairFour);
		assertMapContainsKeyValuePair(map, integerPairFive);
		assertMapContainsKeyValuePair(map, integerPairSix);

		assertFindsEntry(map, integerPairTwo);
		assertFindsEntry(map, integerPairSix);

		addToMapAndAssertMapCount(map, integerPairSeven, 5);

		assertMapContainsKeyValuePair(map, integerPairTwo);
		assertMapContainsKeyValuePair(map, integerPairFour);
		assertMapContainsKeyValuePair(map, integerPairFive);
		assertMapContainsKeyValuePair(map, integerPairSix);
		assertMapContainsKeyValuePair(map, integerPairSeven);

		addToMapAndAssertMapCount(map, integerPairEight, 5);

		assertMapContainsKeyValuePair(map, integerPairTwo);
		assertMapContainsKeyValuePair(map, integerPairFive);
		assertMapContainsKeyValuePair(map, integerPairSix);
		assertMapContainsKeyValuePair(map, integerPairSeven);
		assertMapContainsKeyValuePair(map, integerPairEight);

		addToMapAndAssertMapCount(map, integerPairNine, 5);

		assertMapContainsKeyValuePair(map, integerPairTwo);
		assertMapContainsKeyValuePair(map, integerPairSix);
		assertMapContainsKeyValuePair(map, integerPairSeven);
		assertMapContainsKeyValuePair(map, integerPairEight);
		assertMapContainsKeyValuePair(map, integerPairNine);

		addToMapAndAssertMapCount(map, integerPairTen, 5);

		assertMapContainsKeyValuePair(map, integerPairTwo);
		assertMapContainsKeyValuePair(map, integerPairSix);
		assertMapContainsKeyValuePair(map, integerPairEight);
		assertMapContainsKeyValuePair(map, integerPairNine);
		assertMapContainsKeyValuePair(map, integerPairTen);
	}

	@Test
	public void addItemsToMap_CheckIfAddDuplicateKeyReplacesOldMapping() {
		ForgettingMap<String, Integer> map = new ForgettingMapImpl<>(5);
		assertEquals(0, map.count());

		addToMapAndAssertMapCount(map, stringIntegerPairOne, 1);
		addToMapAndAssertMapCount(map, stringIntegerPairTwo, 2);
		addToMapAndAssertMapCount(map, stringIntegerPairThree, 3);
		addToMapAndAssertMapCount(map, stringIntegerPairFour, 4);
		addToMapAndAssertMapCount(map, stringIntegerPairFive, 5);

		assertMapContainsKeyValuePair(map, stringIntegerPairOne);
		assertMapContainsKeyValuePair(map, stringIntegerPairTwo);
		assertMapContainsKeyValuePair(map, stringIntegerPairThree);
		assertMapContainsKeyValuePair(map, stringIntegerPairFour);
		assertMapContainsKeyValuePair(map, stringIntegerPairFive);

		addToMapAndAssertMapCount(map, stringIntegerPairSix, 5);

		assertMapContainsKeyValuePair(map, stringIntegerPairTwo);
		assertMapContainsKeyValuePair(map, stringIntegerPairThree);
		assertMapContainsKeyValuePair(map, stringIntegerPairFour);
		assertMapContainsKeyValuePair(map, stringIntegerPairFive);
		assertMapContainsKeyValuePair(map, stringIntegerPairSix);

		assertFindsEntry(map, stringIntegerPairThree);
		assertFindsEntry(map, stringIntegerPairSix);

		addToMapAndAssertMapCount(map, stringIntegerPairSeven, 5);

		assertMapContainsKeyValuePair(map, stringIntegerPairThree);
		assertMapContainsKeyValuePair(map, stringIntegerPairFour);
		assertMapContainsKeyValuePair(map, stringIntegerPairFive);
		assertMapContainsKeyValuePair(map, stringIntegerPairSix);
		assertMapContainsKeyValuePair(map, stringIntegerPairSeven);

		addToMapAndAssertMapCount(map, stringIntegerPairEight, 5);

		assertMapContainsKeyValuePair(map, stringIntegerPairFour);
		assertMapContainsKeyValuePair(map, stringIntegerPairFive);
		assertMapContainsKeyValuePair(map, stringIntegerPairSix);
		assertMapContainsKeyValuePair(map, stringIntegerPairSeven);
		assertMapContainsKeyValuePair(map, stringIntegerPairEight);
	}
}
