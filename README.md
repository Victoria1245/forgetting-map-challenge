# The Challenge #

## Objective ##

The objective of this task is design, implement and test a thread-safe 'forgetting map'.

A 'forgetting' map should hold associations between a �key� and some �content�. It should implement at least two methods:

1.	add (add an association)
2.	find (find content using the specified key).

It should hold as many associations as it can, but no more than x associations at any time, with x being a parameter passed to the constructor. The association that is least used is removed from the map when a new entry requires a space and the map is at capacity, where �least used� relates to the number of times each association has been retrieved by the �find� method.  A suitable tie breaker should be used in the case where there are multiple least-used entries.

## Implementation ##

Java would be preferable.

You may use standard collections (HashMap, LinkedList, etc.) to build your solution but the use of a pre-existing library solution with the required behaviour, though an option in everyday work, is not what we�re looking for here.


# First Thoughts #

These types of challenges aren't my strong point but I'll give it a go.  I don't have the background knowledge to know which collection or map implementation might be the most performant or even which one might be the best fit, so I shall go with using a plain HashMap.

The disadvantage with picking HashMap is going to be the need to synchronise every access to the map vs. ConcurrentHashMap where that wouldn't be needed.

# Coding #

My first iteration (can be seen in the first draft checked in) was to implement ConcurrentMap and add in implementations for all the methods that needed overriding.  This ended up wasting quite a bit of time and I ended up abandoning this idea - if I had unlimited time, I might have continued down this route.

The second iteration was to extend ConcurrentHashMap which thinking about it now, might well have worked!  Anyway, I ended up abandoning this idea too...

The third iteration was to implement Map but again, I wasted a lot of time and abandoned it...

By this stage, I think I'd spend about 3 hours on the challenge and not really achieved anything.

The last iteration which ended up being the route I finally took was to ignore implementing Map to extending one of the Map implementations and just create a plain class which had a map field and contained only the required methods as originally requested on the challenge (add+find).

Because no type was mentioned on the challenge, I used generics from the start - as can also be seen in the initial drafts.

Rather than hold two lists/maps, one for the key/value pair and one for the key/counter pair, I chose to only have one map and wrap the value with the wrapper class which could then contain the counter.  This meant access to the value could trigger an increment of the counter and also meant that I didn't have to keep two collections in sync with one another.

# How to run the project #

Since it's a Maven project, `clean test` will download all the relevant dependencies, clean the project and execute the tests :)

## Required Java version ##

11 or greater

# Time Taken #

~ 11 hours: About 9 hours on Friday (writing the bulk of the code) and a further 2 on Sunday (cleaning up the code a little, adding in threaded tests and writing up the readme).
